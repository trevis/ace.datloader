using System.IO;
using Microsoft.DirectX;

namespace ACE.DatLoader.Entity
{
    public class Sphere : IUnpackable
    {
        public Vector3 Origin { get; private set; }
        public float Radius { get; private set; }

        public void Unpack(BinaryReader reader)
        {
            Origin = reader.ReadVector3();
            Radius = reader.ReadSingle();
        }

        public static Sphere CreateDummySphere()
        {
            var sphere = new Sphere();
            sphere.Origin = new Vector3();
            return sphere;
        }
    }
}
