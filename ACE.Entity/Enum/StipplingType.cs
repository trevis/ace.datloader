namespace ACE.Entity.Enum
{
    public enum StipplingType
    {
        None        = 0x0,
        Positive    = 0x1,
        Negative    = 0x2,
        Both        = 0x3,
        NoPos       = 0x4,
        NoNeg       = 0x8,
        NoUVS       = 0x14
    };

    public static class StipplingTypeExtensions {
        public static bool HasFlag(this StipplingType stipplingType, StipplingType flag) {
            return ((uint)stipplingType & (uint)flag) != 0;
        }
    }
}
