using System;

namespace ACE.Entity.Enum
{
    [Flags]
    public enum PortalFlags
    {
        ExactMatch = 0x1,
        PortalSide = 0x2
    }

    public static class PortalFlagsExtensions {
        public static bool HasFlag(this PortalFlags type, PortalFlags flag) {
            return ((uint)type & (uint)flag) != 0;
        }
    }
}
