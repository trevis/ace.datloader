using System;

namespace ACE.Entity.Enum
{
    [Flags]
    public enum EnvCellFlags
    {
        SeenOutside       = 0x1,
        HasStaticObjs     = 0x2,
        HasRestrictionObj = 0x8
    };

    public static class EnvCellFlagsExtensions {
        public static bool HasFlag(this EnvCellFlags type, EnvCellFlags flag) {
            return ((uint)type & (uint)flag) != 0;
        }
    }
}
